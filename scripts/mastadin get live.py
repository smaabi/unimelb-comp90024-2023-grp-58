import couchdb
from mastodon import Mastodon, StreamListener

url = 'http://172.26.136.104:5987/'
db_name = 'mastodon_toots'
username = 'admin'
password = 'admin'

couch = couchdb.Server(url)
couch.resource.credentials = (username, password)

if db_name not in couch:
    couch.create(db_name)

db = couch[db_name]

mastodon1 = Mastodon(
    client_id="oRbdSlVfZY0QZlQGShnOIO_5ij_EhWdSHthv0D7R3Mk",
    client_secret="YhUrTMSGFFkllgduop7qpcjsDrEKPUowWZbg7AnP5RE",
    access_token="TQ31tIhNkzYB-igvHgwDu0WqDa83Rf9ilzHRCEUd638",
    api_base_url="https://mastodon.au"
)
mastodon2 = Mastodon(
    client_id="QVDetdHXYa0h4HIlbrkEhI9QsAD1ubeQExNFLEBmPZw",
    client_secret="2Pim45HGPpOjKyH4f4-xNdAlIfVBELh4JLtJMWb-uH0",
    access_token="Z78RnLw3VQrmde0jGtyMBs8fWn0Rqk8xdctjhzuvTNE",
    api_base_url="https://mastodon.social"
)
class HashtagListener(StreamListener):
    def on_update(self, status):
        hashtags = status['tags']
        desired_hashtags = ['melbourne','victoria']  
        content = status['content'].lower()

        if any(tag['name'] in desired_hashtags for tag in hashtags):
            acct = status['account']['acct']
            acct_id = status['account']['id']
            lang = status['language']

            extracted_hashtags = [tag['name'] for tag in hashtags]
            doc = {
                'acct_name': acct,
                'acct_id': acct_id,
                'content': content,
                'language': lang,
                'hashtags': extracted_hashtags
            }
            db.save(doc)
            print("Saved to CouchDB:", doc)

listener = HashtagListener()
mastodon1.stream_public(listener=listener)  
mastodon2.stream_public(listener=listener)  
