import ijson
import json
import re
import zipfile
from langdetect import detect
def print_next_lines(filename, start_line):
    st="melbourne"
    with open("D:\\fi\\part5.json", 'w', encoding='utf-8') as fo:
        with open(filename, 'r',encoding='utf-8') as file:
            parser = ijson.parse(file)
            location = ''
            text = ''
            line_number = 0
            lang=''
            flag=0
            for prefix, event, value in parser:
                line_number += 1
                if prefix.endswith('.lang') and event == 'string':
                    lang = value
                if prefix.endswith('.full_name') and event == 'string':
                    location = str(value)

                if prefix.endswith('.text') and event == 'string' and lang=="en" and location.endswith(', Victoria'):
                    text = str(value)
                    if(st not in  location.lower()):
                        hashtags = re.findall(r'#\w+', text, flags=re.UNICODE)
                        if line_number > start_line:
                            doc = {
                                "location": location,
                                "text": text,
                                "hashtags": hashtags
                            }
                            json.dump(doc, fo, indent=4)
                            fo.write(",\n")
                        if line_number == start_line + 25000:
                            break
                    if(st not in  location.lower()):
                        flag=1
# Provide the path to your JSON file and the starting line number
json_file=r"D:\fi\twitter-huge.json"
starting_line = 100000

print_next_lines(json_file, starting_line)
print("done")
