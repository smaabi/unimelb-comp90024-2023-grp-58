#!/usr/bin/env bash

. ./openrc.sh; ansible-playbook -vv -i hosts--ask-become-pass setup.yaml --check | tee output.txt
